# Ofi Test

- Author: Andres Rodriguez
- Stack: NodeJS 8+, Yarn, VueJS, NuxtJS.
- Styles: Scss, Bulma Framework. 

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate

# test project
$ yarn test
```