import { resolve } from 'path'
import { Nuxt, Builder } from 'nuxt'
import { JSDOM } from 'jsdom'
import test from 'ava'
import fs from 'fs'
import ofi from './assets/scripts/ofi'

let nuxt = null

test.before('Init server', async t => {
  const rootDir = resolve(__dirname, './')
  let config = {}
  try { config = require(resolve(rootDir, 'nuxt.config.js')) } catch (e) { }
  config.rootDir = rootDir
  config.dev = false
  config.mode = 'universal'
  nuxt = new Nuxt(config)
  await new Builder(nuxt).build()
  nuxt.listen(4000, 'localhost')
}, 30000)

test('Ofi data creation', async (t) => {
  const path = resolve(__dirname, './static/products.json');
  if (fs.existsSync(path)) {
    fs.unlinkSync(path)
  }
  ofi.run()
  // Verifica que el archivo haya sido creado
  t.assert(fs.existsSync(path))
  // Verifica que el formato del archivo json sea correcto
  t.assert(json = JSON.parse(fs.readFileSync(path)))
})

test('Rederizado en el servidor, OK', async (t) => {
  const context = {}
  const { html } = await nuxt.server.renderRoute('/', context)
  t.true(html.includes('<h1 class="title">Ofi.</h1>'))
})

test.after('Closing server and nuxt.js', (t) => {
  nuxt.close()
})
