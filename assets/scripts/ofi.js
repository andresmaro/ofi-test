const fs = require('fs')
const path = require('path')
const consola = require('consola')
const _ = require('lodash')

const dir = path.resolve(__dirname, '../products')

function run() {
  consola.info({ message: '[ofi] Praparando datos ' })

  let products = new Set()
  let categories = new Set()
  let industries = new Set()

  files = fs.readdirSync(dir);
  files.forEach(file => {
    json = null
    try {
      file = fs.readFileSync(path.resolve(dir, file), "utf-8")
      json = JSON.parse(file)[0]

    } catch (e) {
      if (e instanceof SyntaxError) {
        /* 
          Este es un fix para algunos archivos con mal formato JSON, pero lo correcto sería corregir 
          la fuente que los está generando mal.
        */
        file += "]"
        try {
          json = JSON.parse(file)[0]
        } catch (e) {
          console.log(e)
        }
      }

    }
    if (json) {
      products.add(json)
      categories.add(json.category.trim())
      industries.add(json.industry.trim())
    }

  });

  products = _.orderBy([...products], 'name')

  data = { categories: [...categories].sort(), industries: [...industries].sort(), products: products }

  fs.writeFileSync(path.resolve(__dirname, '../../static/products.json'), JSON.stringify(data))

  consola.success({ message: '[ofi] terminando' })
}

module.exports = {
  run: run
}
